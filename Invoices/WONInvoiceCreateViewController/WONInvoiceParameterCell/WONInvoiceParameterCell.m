

#import "WONInvoiceParameterCell.h"

const CGFloat cWONInvoicePickerViewHeight = 217.f;

@interface WONInvoiceParameterCell () <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak,   nonatomic) IBOutlet UILabel      *parameterTitleLabel;
@property (weak,   nonatomic) IBOutlet UILabel      *parameterValueLabel;
@property (weak,   nonatomic) IBOutlet UITextField  *valueTextField;
@property (weak,   nonatomic) IBOutlet UIImageView  *disclosureImageView;
@property (strong, nonatomic)          UIPickerView *pickerView;

@end

@implementation WONInvoiceParameterCell

@dynamic title, value, placeholder;

#pragma mark - Custom Accessors

#pragma mark Getters
- (UIPickerView *)pickerView {
    if (!_pickerView) {
        _pickerView = [UIPickerView new];
        _pickerView.delegate = self;
        _pickerView.dataSource = self;
        [self.contentView addSubview:_pickerView];
    }
    return _pickerView;
}

- (NSString *)value {
    return [self.parameterValueLabel text];
}

- (NSString *)placeholder {
    return [self.valueTextField placeholder];
}

- (NSString *)title {
    return [self.parameterTitleLabel text];
}

#pragma mark Setters
- (void)setPlaceholder:(NSString *)placeholder {
    if (self.valueTextField.placeholder == placeholder) {
        return;
    }
    self.valueTextField.placeholder = placeholder;
}

- (void)setTitle:(NSString *)title {
    if (self.parameterTitleLabel.text == title) {
        return;
    }
    self.parameterTitleLabel.text = title;
}

- (void)setValue:(NSString *)value {
    if (self.parameterValueLabel.text == value) {
        return;
    }
    self.parameterValueLabel.text = value;
    self.valueTextField.text = value;
}

- (void)setKeyboard:(UIKeyboardType)keyboard {
    if (_keyboard == keyboard) {
        return;
    }
    _keyboard = keyboard;
    self.valueTextField.keyboardType = keyboard;
}

- (void)setEditable:(BOOL)editable {
    if (_editable == editable) {
        return;
    }
    _editable = editable;
    
    [self configurateCell];
}

- (void)setHasDatePicker:(BOOL)hasDatePicker {
    if (_hasDatePicker == hasDatePicker) {
        return;
    }
    _hasDatePicker = hasDatePicker;
    self.valueTextField.userInteractionEnabled = !hasDatePicker;
}

#pragma mark - Public

#pragma mark - Private

- (void)configurateCell {
    if (!self.hasDatePicker) {
        if (self.editable) {
            self.valueTextField.userInteractionEnabled = YES;
            [self.valueTextField becomeFirstResponder];
        } else {
            [self.valueTextField resignFirstResponder];
            self.valueTextField.userInteractionEnabled = NO;
        }
        BOOL hasValue = (self.valueTextField.text.length > 0);
        self.parameterValueLabel.hidden = self.editable && hasValue;
        self.valueTextField.hidden = !self.editable && hasValue;
    } else {
        if (self.editable) {
            [self showDatePicker];
        } else {
            [self hideDatePicker];
        }
        self.parameterValueLabel.hidden = NO;
        self.valueTextField.hidden = YES;
    }
}

- (void)showDatePicker {
    self.pickerView.hidden = NO;
    self.backgroundColor = [UIColor wonNavigationBarTitleBlueColor];//wonScheduleDatePickerBackgroundColor];
    _parameterTitleLabel.textColor = [UIColor whiteColor];
    _parameterValueLabel.textColor = [UIColor whiteColor];
    [self.pickerView autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:self.parameterValueLabel withOffset:0 relation:NSLayoutRelationEqual];
    [self.pickerView autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:self withOffset:0 relation:NSLayoutRelationEqual];
    [self.pickerView autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:self withOffset:0 relation:NSLayoutRelationEqual];
}

- (void)hideDatePicker {
    self.backgroundColor = [UIColor clearColor];
    _parameterTitleLabel.textColor = [UIColor wonProfileCancelButtonColor];
    _parameterValueLabel.textColor = [UIColor wonProfileAttributeValueColor];
    NSInteger row = [self.pickerView selectedRowInComponent:0];
    NSString *value = [self.delegate pickerRowsInvoiceParameterCell:self][row];
    [self.delegate invoiceParameterCell:self didEndValueEditing:value];
    self.value = value;
    
    self.pickerView.hidden = YES;
}

#pragma mark - WONTableViewCell

+ (CGFloat)cellHeight
{
    return 60.f;
}

#pragma mark - NSObject

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    _parameterTitleLabel.font = [UIFont wonOpenSansWithStyle:WONFontOpenSansStyleRegular andSize:12.0f];
    _parameterValueLabel.font = [UIFont wonOpenSansWithStyle:WONFontOpenSansStyleRegular andSize:15.0f];
    _valueTextField.font = [UIFont wonOpenSansWithStyle:WONFontOpenSansStyleRegular andSize:15.0f];
    _valueTextField.delegate = self;
    _valueTextField.userInteractionEnabled = NO;
    
    _parameterTitleLabel.textColor = [UIColor wonProfileCancelButtonColor];
    _parameterValueLabel.textColor = [UIColor wonProfileAttributeValueColor];
    
    if ([self.valueTextField respondsToSelector:@selector(setTintColor:)]) {
        self.valueTextField.tintColor = [UIColor wonYellowColor];
    }
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    UITextPosition *position = [textField endOfDocument];
    [textField setSelectedTextRange:[textField textRangeFromPosition:position toPosition:position]];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.value = textField.text;
    [self.delegate invoiceParameterCell:self didEndValueEditing:textField.text];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.delegate pickerRowsInvoiceParameterCell:self].count;
}

#pragma mark - UIPickerViewDelegate

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel *pickerLabel = (UILabel *)view;
    if (!pickerLabel) {
        pickerLabel= [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [pickerView rowSizeForComponent:component].width, [pickerView rowSizeForComponent:component].height)];
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        pickerLabel.textColor = [UIColor whiteColor];
        pickerLabel.font = [pickerLabel.font fontWithSize:20];
    }
    pickerLabel.text = [self.delegate pickerRowsInvoiceParameterCell:self][row];
    
    return pickerLabel;
}

@end
