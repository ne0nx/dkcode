

#import "WONTableViewCell.h"

@class WONInvoiceParameterCell;

@protocol WONInvoiceParameterCellDelegate <NSObject>

- (void)invoiceParameterCell:(WONInvoiceParameterCell *)cell didEndValueEditing:(id)value;

@required
- (NSArray *)pickerRowsInvoiceParameterCell:(WONInvoiceParameterCell *)cell;

@end

FOUNDATION_EXTERN const CGFloat cWONInvoicePickerViewHeight;

@interface WONInvoiceParameterCell : WONTableViewCell

@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *value;
@property (copy, nonatomic) NSString *placeholder;

@property (weak, nonatomic) id<WONInvoiceParameterCellDelegate> delegate;

@property (assign, nonatomic) BOOL hasDatePicker;
@property (assign, nonatomic) BOOL editable;
@property (assign, nonatomic) UIKeyboardType keyboard;

@end
