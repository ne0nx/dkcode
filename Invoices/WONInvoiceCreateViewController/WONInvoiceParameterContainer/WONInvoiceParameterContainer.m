//
//  WONInvoiceParameterContainer.m
//  unistream
//
//  Created by Dmitriy Karachentsov on 04.09.14.
//  Copyright (c) 2014 WalletOne. All rights reserved.
//

#import "WONInvoiceParameterContainer.h"

@interface WONInvoiceParameterContainer ()
@property (nonatomic, copy, readwrite) NSString *title;
@property (nonatomic, copy, readwrite) NSString *placeholder;
@end

@implementation WONInvoiceParameterContainer

#pragma mark - Lifecycle

+ (instancetype)containerWithType:(WONInvoiceParameterType)type {
    return [[[self class] alloc] initWithType1:type];
}

- (instancetype)initWithType1:(WONInvoiceParameterType)type
{
    self = [super init];
    if (self) {
        self.parameterType = type;
    }
    return self;
}

#pragma mark - Custom Accessors

- (void)setOutgoingInvoice:(WONOutgoingInvoice *)outgoingInvoice {
    if (_outgoingInvoice == outgoingInvoice) {
        return;
    }
    _outgoingInvoice = outgoingInvoice;
    self.title = [[self class] parameterTitleWithType:self.parameterType];
    self.placeholder = [[self class] parameterPlaceholderWithType:self.parameterType];
}

#pragma mark - Public

- (void)changeValueWithObject:(id)object {
    switch (self.parameterType) {
        case WONInvoiceParameterTypeRecipient:
            self.outgoingInvoice.recipient = object;
            break;
        case WONInvoiceParameterTypeAmount:
            self.outgoingInvoice.amount = [object floatValue];
            break;
        case WONInvoiceParameterTypeExpireDate:
            self.outgoingInvoice.interval = [object integerValue];
            break;
        case WONInvoiceParameterTypeComment:
            self.outgoingInvoice.invoiceDescription = object;
            break;
    }
}

#pragma mark - Private

+ (NSString *)parameterTitleWithType:(WONInvoiceParameterType)type {
    NSString *title = nil;
    switch (type) {
        case WONInvoiceParameterTypeRecipient:
            title = LOC(@"invoice.create.recipient");
            break;
        case WONInvoiceParameterTypeAmount:
            title = LOC(@"invoice.create.amount");
            break;
        case WONInvoiceParameterTypeExpireDate:
            title = LOC(@"invoice.create.expireDate");
            break;
        case WONInvoiceParameterTypeComment:
            title = LOC(@"invoice.create.comment");
            break;
    }
    return title;
}

+ (NSString *)parameterPlaceholderWithType:(WONInvoiceParameterType)type {
    NSString *title = nil;
    switch (type) {
        case WONInvoiceParameterTypeRecipient:
            title = LOC(@"invoice.create.placeholder.recipient");
            break;
        case WONInvoiceParameterTypeAmount:
            title = LOC(@"invoice.create.placeholder.amount");
            break;
        case WONInvoiceParameterTypeExpireDate:
            title = LOC(@"invoice.create.placeholder.expireDate");
            break;
        case WONInvoiceParameterTypeComment:
            title = LOC(@"invoice.create.placeholder.comment");
            break;
    }
    return title;
}


@end
