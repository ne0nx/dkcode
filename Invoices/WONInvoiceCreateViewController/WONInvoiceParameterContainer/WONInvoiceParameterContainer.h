//
//  WONInvoiceParameterContainer.h
//  unistream
//
//  Created by Dmitriy Karachentsov on 04.09.14.
//  Copyright (c) 2014 WalletOne. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WONOutgoingInvoice.h"

typedef NS_ENUM(NSInteger, WONInvoiceParameterType) {
    WONInvoiceParameterTypeRecipient,
    WONInvoiceParameterTypeAmount,
    WONInvoiceParameterTypeExpireDate,
    WONInvoiceParameterTypeComment
};

@interface WONInvoiceParameterContainer : NSObject

@property (nonatomic, assign) WONInvoiceParameterType parameterType;
@property (nonatomic,   weak) WONOutgoingInvoice      *outgoingInvoice;

@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, copy, readonly) NSString *placeholder;
@property (nonatomic, copy) NSString *value;

@property (nonatomic, assign) BOOL editable;

+ (instancetype)containerWithType:(WONInvoiceParameterType)type;

- (void)changeValueWithObject:(id)object;

@end
