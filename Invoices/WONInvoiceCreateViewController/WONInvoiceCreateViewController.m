

#import "WONInvoiceCreateViewController.h"
#import "WONInvoicesViewController.h"

#import "WONInvoiceParameterCell.h"
#import "WONRoundedButton.h"
#import <TPKeyboardAvoidingTableView.h>

#import "WONInvoiceParameterContainer.h"

@interface WONInvoiceCreateViewController () <UITableViewDataSource, UITableViewDelegate, WONTableViewDataSourceProtocol, WONInvoiceParameterCellDelegate>

@property (copy, readwrite, nonatomic) WONInvoiceViewControllerSuccessBlock successBlock;

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingTableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet WONRoundedButton *sendButton;

@property (strong, nonatomic) NSIndexPath *editingCellIndexPath;

@end

@implementation WONInvoiceCreateViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView configureWithCellClass:[WONInvoiceParameterCell class]];
    
    [self.tableView reloadData];
    
    
}

#pragma mark - IBActions 

- (IBAction)sendButtonAction:(id)sender {
    [self setupEditable:NO forRowAtIndexPath:self.editingCellIndexPath];
    @weakify(self);
    [self.dataSource.outgoingInvoice requestCreateInvoiceWithCompletionBlock:^{
        @strongify(self);
        if (self.successBlock) {
            self.successBlock();
        }
    } errorBlock:^(NSError *error) {
        [[WONNotificationBar notificationBarError] presentNotificationBarWithError:error];
    }];
}

#pragma mark - Custom Accessors

#pragma mark Getters

- (WONInvoiceCreateDataSource *)dataSource {
    if (!_dataSource) {
        _dataSource = [WONInvoiceCreateDataSource new];
        _dataSource.delegate = self;
    }
    return _dataSource;
}

#pragma mark Setters

#pragma mark - Public

+ (instancetype)invoiceCreateControllerWithSuccessBlock:(WONInvoiceViewControllerSuccessBlock)successBlock {
    WONInvoiceCreateViewController *controller = [[[self class] alloc] initWithNibName:@"WONInvoiceCreateViewController" bundle:nil];
    controller.successBlock = successBlock;
    return controller;
}

#pragma mark - Private

- (void)setupEditable:(BOOL)editable forRowAtIndexPath:(NSIndexPath *)indexPath {
    WONInvoiceParameterContainer *container = [self.dataSource objectForIndexPath:indexPath];
    WONInvoiceParameterCell *cell = (id)[self.tableView cellForRowAtIndexPath:indexPath];
    
    container.editable = editable;
    [self.tableView beginUpdates];
    cell.editable = editable;
    [self.tableView endUpdates];
    self.editingCellIndexPath = editable ? indexPath : nil;
}

#pragma mark Overriden
- (void)adjustInterface
{
    [super adjustInterface];
    self.tableView.tableFooterView = [self footerView];
}

- (void)localizeTexts
{
    [super localizeTexts];
    [self setNavigationBarTitle:LOC(@"invoice.create.navigation_bar.title")];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataSource rowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WONInvoiceParameterCell *cell = [tableView dequeueReusableCellWithIdentifier:[WONInvoiceParameterCell reuseIdentifier] forIndexPath:indexPath];
    cell.delegate = self;
    
    WONInvoiceParameterContainer *container = [self.dataSource objectForIndexPath:indexPath];
    cell.title = [container title];
    cell.value = [container value];
    cell.hasDatePicker = (container.parameterType == WONInvoiceParameterTypeExpireDate);
    cell.editable = container.editable;
    cell.placeholder = container.placeholder;
    
    if (container.parameterType == WONInvoiceParameterTypeAmount) {
        cell.keyboard = UIKeyboardTypeDecimalPad;
    } else if (container.parameterType == WONInvoiceParameterTypeRecipient){
        cell.keyboard = UIKeyboardTypePhonePad;
    } else {
        cell.keyboard = UIKeyboardTypeDefault;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (![self.editingCellIndexPath isEqual:indexPath]) {
        if (self.editingCellIndexPath) {
            [self setupEditable:NO forRowAtIndexPath:self.editingCellIndexPath];
        }
        [self setupEditable:YES forRowAtIndexPath:indexPath];
    } else {
        [self setupEditable:NO forRowAtIndexPath:indexPath];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat rowHeight = [WONInvoiceParameterCell cellHeight];
    WONInvoiceParameterContainer *container = [self.dataSource objectForIndexPath:indexPath];
    if (container.editable
        && container.parameterType == WONInvoiceParameterTypeExpireDate) {
        rowHeight += cWONInvoicePickerViewHeight;
    }
    return rowHeight;
}

#pragma mark - WONTableViewDataSourceProtocol

- (void)dataSourceDidChanged:(WONBaseDataSource *)dataSource {
    [self.tableView reloadData];
}

- (void)dataSourceDidFinishLoading:(WONBaseDataSource *)dataSource {
}

- (void)dataSourceDidStartLoading:(WONBaseDataSource *)dataSource {
}

#pragma mark - WONInvoiceParameterCellDelegate

-(void)invoiceParameterCell:(WONInvoiceParameterCell *)cell didEndValueEditing:(id)value {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    WONInvoiceParameterContainer *container = [self.dataSource objectForIndexPath:indexPath];
    if (container.parameterType == WONInvoiceParameterTypeExpireDate) {
        WONOutgoingInvoiceInterval date = [WONOutgoingInvoice dateWithString:value];
        [container changeValueWithObject:@(date)];
    } else {
        [container changeValueWithObject:value];
    }
}

- (NSArray *)pickerRowsInvoiceParameterCell:(WONInvoiceParameterCell *)cell {
    return @[[WONOutgoingInvoice dateStringWithDate:WONOutgoingInvoiceIntervalOneDay],
             [WONOutgoingInvoice dateStringWithDate:WONOutgoingInvoiceIntervalThreeDays],
             [WONOutgoingInvoice dateStringWithDate:WONOutgoingInvoiceIntervalSevenDays]];
}

@end
