
#import <Foundation/Foundation.h>
#import "WONBaseDataSource.h"
#import "WONOutgoingInvoice.h"

@interface WONInvoiceCreateDataSource : WONBaseDataSource

@property (nonatomic, strong) WONOutgoingInvoice *outgoingInvoice;

@end
