

#import "WONInvoiceCreateDataSource.h"
#import "WONInvoiceParameterContainer.h"

@implementation WONInvoiceCreateDataSource

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self prepareContainers];
    }
    return self;
}

#pragma mark - Custom Accessors

#pragma mark - Getter
- (WONOutgoingInvoice *)outgoingInvoice {
    if (!_outgoingInvoice) {
        _outgoingInvoice = [WONOutgoingInvoice new];
        _outgoingInvoice.invoiceDescription = @"";
        _outgoingInvoice.recipient = @"";
    }
    return _outgoingInvoice;
}

#pragma mark - Private

- (NSArray *)parameters {
    return @[@(WONInvoiceParameterTypeRecipient),@(WONInvoiceParameterTypeAmount),@(WONInvoiceParameterTypeExpireDate),@(WONInvoiceParameterTypeComment)];
}

- (void)prepareContainers {
    NSMutableArray *containers = [NSMutableArray array];
    
    for (NSNumber *number in self.parameters) {
        WONInvoiceParameterType type = [number integerValue];
        WONInvoiceParameterContainer *container = [WONInvoiceParameterContainer containerWithType:type];
        container.outgoingInvoice = [self outgoingInvoice];
        [containers addObject:container];
    }
    
    WONSectionDescriptionObject *section = [[WONSectionDescriptionObject alloc] initWithTitleString:nil rowsArray:containers];
    [self.arrayOfSectionDescriptions removeAllObjects];
    [self.arrayOfSectionDescriptions addObject:section];
    
    [self.delegate dataSourceDidChanged:self];
}

@end
