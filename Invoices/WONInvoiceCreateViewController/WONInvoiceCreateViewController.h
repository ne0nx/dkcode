

#import "WONViewController.h"
#import "WONInvoiceCreateDataSource.h"

typedef void (^WONInvoiceViewControllerSuccessBlock)(void);

@interface WONInvoiceCreateViewController : WONViewController

@property (strong, nonatomic)           WONInvoiceCreateDataSource                    *dataSource;
@property (copy,   readonly, nonatomic) WONInvoiceViewControllerSuccessBlock          successBlock;

+ (instancetype)invoiceCreateControllerWithSuccessBlock:(WONInvoiceViewControllerSuccessBlock)successBlock;

@end
