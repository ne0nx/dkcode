

#import "WONInvoicePaymentPopupView.h"

@implementation WONInvoicePaymentPopupView {
    struct {
        unsigned int invoicePopupWillOpen : 1;
        unsigned int invoicePopupDidClose : 1;
        unsigned int invoicePopupConfirm  : 1;
        unsigned int invoicePopupReject   : 1;
    } delegateResponds;
}

#pragma mark - 

+ (instancetype)popupViewWithType:(WONPopupType)type dismissCompletion:(WONPopupDismissCompletion)dismissCompletion {
    WONInvoicePaymentPopupView *popupView = [super popupViewWithDismissCompletion:dismissCompletion];
    popupView.type = type;
    return popupView;
}

#pragma mark - Custom Accessors

- (void)setDelegate:(id<WONInvoicePaymentPopupDelegate>)delegate {
    _delegate = delegate;
    delegateResponds.invoicePopupDidClose = [_delegate respondsToSelector:@selector(invoicePopupDidClose:)];
    delegateResponds.invoicePopupWillOpen = [_delegate respondsToSelector:@selector(invoicePopupWillOpen:)];
    delegateResponds.invoicePopupConfirm  = [_delegate respondsToSelector:@selector(invoicePopupConfirm:)];
    delegateResponds.invoicePopupReject   = [_delegate respondsToSelector:@selector(invoicePopupReject:)];
}

- (void)setType:(WONPopupType)type {
    _type = type;
    [self configuratePopup];
}

#pragma mark - IBActions

- (IBAction)topBtnAction:(id)sender {
    if (self.type == WONPopupTypeInvoiceRejectedOrAccepted) {
        [self hidePopup];
    } else if (self.type == WONPopupTypeInvoiceReceived) {
        if (delegateResponds.invoicePopupConfirm) {
            [self.delegate invoicePopupConfirm:self];
        }
    } else if (self.type == WONPopupTypeInvoiceOutgoing) {
        if (delegateResponds.invoicePopupReject) {
            [self.delegate invoicePopupReject:self];
        }
    }
}

- (IBAction)bottomBtnAction:(id)sender {
    if (self.type == WONPopupTypeInvoiceReceived) {
        if (delegateResponds.invoicePopupReject) {
            [self.delegate invoicePopupReject:self];
        }
    }
}

#pragma mark - Private

- (void)updatePopup {
    CGSize newSize = [self systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    [self setNewHeight:newSize.height];
    [self updatePosition];
}

- (void)setupRAC {
    void (^updatePopup)(id) = ^(id x){
        [self updatePopup];
    };
    
    [RACObserve(self, self.titleLabel.text) subscribeNext:updatePopup];
    [RACObserve(self, self.dateLabel.text) subscribeNext:updatePopup];
    [RACObserve(self, self.descriptionLabel.text) subscribeNext:updatePopup];
}

- (void)configuratePopup {
    switch (self.type) {
        case WONPopupTypeInvoiceOutgoing:
            [self configurateOutgoingPopup];
            break;
        case WONPopupTypeInvoiceReceived:
            [self configurateReceivedPopup];
            break;
        case WONPopupTypeInvoiceRejectedOrAccepted:
            [self configurateRejectedOrPaidPopup];
            break;
        default:
            break;
    }
}

- (void)configurateOutgoingPopup {
    [self removeSecondBtn];
    [self.firstBtn setTitle:LOC(@"invoice.popup.revoke") forState:UIControlStateNormal];
}

- (void)configurateReceivedPopup {
    [self.firstBtn setTitle:LOC(@"invoice.popup.paid") forState:UIControlStateNormal];
    [self.secondBtn setTitle:LOC(@"invoice.popup.reject") forState:UIControlStateNormal];
}

- (void)configurateRejectedOrPaidPopup {
    [self removeSecondBtn];
    [self.firstBtn setTitle:LOC(@"invoice.popup.ok") forState:UIControlStateNormal];
}

- (void)removeSecondBtn {
    [self.secondBtn removeFromSuperview];
    [self autoPinEdge:ALEdgeBottom toEdge:ALEdgeBottom ofView:self.firstBtn withOffset:10.f];
    
    [self updatePopup];
}

#pragma mark - WONPopupView

- (void)showPopup {
    if (delegateResponds.invoicePopupWillOpen) {
        [self.delegate invoicePopupWillOpen:self];
    }
    [super showPopup];
}

- (void)hidePopup {
    [super hidePopup];
    if (delegateResponds.invoicePopupDidClose) {
        [self.delegate invoicePopupDidClose:self];
    }
}

#pragma mark - NSObject

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.titleLabel.font        = [UIFont wonOpenSansWithStyle:WONFontOpenSansStyleRegular andSize:17.0f];
    self.dateLabel.font         = [UIFont wonOpenSansWithStyle:WONFontOpenSansStyleRegular andSize:12.5f];
    self.descriptionLabel.font  = [UIFont wonOpenSansWithStyle:WONFontOpenSansStyleRegular andSize:12.5f];
    
    [self setupRAC];
}

@end
