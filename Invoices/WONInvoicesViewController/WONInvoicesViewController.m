

#import "WONInvoicesViewController.h"
#import "WONInvoiceCreateViewController.h"

#import "WONListHeaderView.h"
#import "WONInvoiceViewCell.h"
#import "WONBarAddItem.h"
#import "UIView+ActivityIndicator.h"
#import "WONInvoicePaymentPopupView.h"

#import "WONPaymentInvoice.h"
#import "WONInvoiceContainer.h"

@interface WONInvoicesViewController () <UITableViewDataSource, UITableViewDelegate, WONInvoicePaymentPopupDelegate>

@property (weak, nonatomic) WONBarAddItem *addBarItem;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) WONPaymentInvoice *paymentInvoice;

@end

@implementation WONInvoicesViewController

@synthesize dataSource = _dataSource;

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.tableView configureWithCellClass:[WONInvoiceViewCell class]];

    [self.dataSource loadData];
}

#pragma mark Overriden
- (void)customizeNavigationbarRightButton
{
    [self addAddBarItem];
}

#pragma mark - Custom Accessors

- (WONInvoicesDataSource *)dataSource {
    if (!_dataSource) {
        _dataSource = [WONInvoicesDataSource new];
        _dataSource.delegate = self;
    }
    return _dataSource;
}

#pragma mark - Private

- (void)configurateAndShowPopupWithInvoiceContainer:(WONInvoiceContainer *)container {
    WONInvoicePaymentPopupView *popupView = [WONInvoicePaymentPopupView popupViewWithDismissCompletion:nil];
    popupView.delegate = self;
    popupView.titleLabel.text = [container formattedTitle];
    popupView.dateLabel.text = [container formattedTime];
    popupView.descriptionLabel.text = [container.invoice invoiceDescription];

    switch (container.invoice.state) {
        case WONInvoiceStateAccepted:
        case WONInvoiceStateCanceled:
        case WONInvoiceStateExpired:
        case WONInvoiceStateRejected:
            popupView.type = WONPopupTypeInvoiceRejectedOrAccepted;
            break;
        default:
            if (container.invoice.direction == WONInvoiceDirectionInc) {
                popupView.type = WONPopupTypeInvoiceReceived;
            } else if (container.invoice.direction == WONInvoiceDirectionOut) {
                popupView.type = WONPopupTypeInvoiceOutgoing;
            }
            break;
    }
    [popupView showPopup];
}

- (WONInvoicePaymentPopupView *)successPopupWithInvoiceContainer:(WONInvoiceContainer *)container {
    NSString *invoiceTitle = [container formattedTitle];
    WONInvoicePaymentPopupView *popupView = [WONInvoicePaymentPopupView popupViewWithType:WONPopupTypeInvoiceCustom dismissCompletion:nil];
    popupView.titleLabel.text = [NSString stringWithFormat:LOC(@"invoice.popup.success_message"),invoiceTitle];
    return popupView;
}

- (void)addAddBarItem {
    WONBarAddItem *addItem = [[NSBundle mainBundle] loadNibNamed:@"WONBarAddItem" owner:nil options:nil][0];
    self.rightBarButtonItems = @[addItem];
    addItem.delegate = self;
    self.addBarItem = addItem;
}

#pragma mark Overriden
- (void)adjustInterface
{
    [super adjustInterface];
}

- (void)localizeTexts
{
    [super localizeTexts];
    [self setNavigationBarTitle:LOC(@"invoices.navigation_bar.title")];
}

#pragma mark Configurate Cells
- (WONInvoiceViewCell *)invoiceCellAtIndexPath:(NSIndexPath *)indexPath {
    WONInvoiceViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:[WONInvoiceViewCell reuseIdentifier]
                                                               forIndexPath:indexPath];
    [self configureInvoiceCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureInvoiceCell:(WONInvoiceViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    WONInvoiceContainer *container = [self.dataSource objectForIndexPath:indexPath];

    [self setTitleForCell:cell invoiceContainer:container];
    [self setTimeForCell:cell invoiceContainer:container];
    [self setAmountForCell:cell invoiceContainer:container];
    [self setStatusImageForCell:cell invoiceContainer:container];
}

- (void)setTitleForCell:(WONInvoiceViewCell *)cell invoiceContainer:(WONInvoiceContainer *)invoiceContainer {
    cell.titleLabel.text = [invoiceContainer formattedTitle];
}

- (void)setTimeForCell:(WONInvoiceViewCell *)cell invoiceContainer:(WONInvoiceContainer *)invoiceContainer {
    cell.timeLabel.text = [invoiceContainer formattedTime];
}

- (void)setAmountForCell:(WONInvoiceViewCell *)cell invoiceContainer:(WONInvoiceContainer *)invoiceContainer {
    cell.amountLabel.textColor = [invoiceContainer invoiceColorFromState];
    cell.amountLabel.text = [invoiceContainer formattedAmount];
}

- (void)setStatusImageForCell:(WONInvoiceViewCell *)cell invoiceContainer:(WONInvoiceContainer *)invoiceContainer {
    cell.statusImageView.image = [invoiceContainer invoiceImageFromState];
}

#pragma mark - <UITableViewDataSource>

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataSource.sectionsCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [self.dataSource rowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self invoiceCellAtIndexPath:indexPath];
}

#pragma mark - <UITableViewDelegate>

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;

    WONInvoiceContainer *container = [self.dataSource objectForIndexPath:indexPath];
    [self configurateAndShowPopupWithInvoiceContainer:container];
}

#pragma mark - <WONTableViewDataSourceProtocol>

- (void)dataSourceDidChanged:(WONBaseDataSource *)dataSource {
    [self.tableView reloadData];

    if ([self.dataSource sectionsCount] == 0) {
        self.tableView.tableFooterView = [UIView noResultsViewWithText:LOC(@"invoices.no_result")
                                                       backgroundColor:[UIColor clearColor]
                                                                  size:CGSizeMake(self.view.width, 128.0f)];
    }
}

- (void)dataSourceDidFinishLoading:(WONBaseDataSource *)dataSource {
    UIView *footerView = self.tableView.tableFooterView;

    [UIView animateWithDuration:0.5 animations:^{
        footerView.alpha = 0.0f;
    } completion:^(BOOL finished) {
        if (self.tableView.tableFooterView == footerView) {
            self.tableView.tableFooterView = nil;
        }
    }];
}

- (void)dataSourceDidStartLoading:(WONBaseDataSource *)dataSource {
    self.tableView.tableFooterView = [UIView activityIndicatorViewWithBackgroundColor:[UIColor clearColor] size:CGSizeMake(self.view.width, 128.0f)];
}

#pragma mark - <WONBarButtonItemDelegate>

- (void)barItemDidTapAdd:(WONBarButtonItem *)barItem {
    if (self.addBarItem == barItem) {
        WONInvoiceCreateViewController *createInvoiceViewController = [WONInvoiceCreateViewController invoiceCreateControllerWithSuccessBlock:^{
            [self.dataSource forceReloadInvoices];
            [self.navigationController popViewControllerAnimated:YES withCompletionBlock:nil];
        }];
        [self.navigationController pushViewController:createInvoiceViewController animated:YES withCompletionBlock:nil];
    }
}

#pragma mark - <WONInvoicePaymentPopupDelegate>

- (void)invoicePopupWillOpen:(WONInvoicePaymentPopupView *)popupView {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    if (!indexPath) {
        return;
    }

    if (popupView.type == WONPopupTypeInvoiceReceived) {
        [SVProgressHUD show];
        [WONPaymentInvoice requestInvoicePaymentWithCompletion:^(WONPaymentInvoice *invoicePayment, NSError *error) {
            [SVProgressHUD dismiss];
            if (error != nil) {
                [[WONNotificationBar notificationBarError] presentNotificationBarWithError:error];
                return;
            }
            self.paymentInvoice = invoicePayment;
        }];
    }
}

- (void)invoicePopupDidClose:(WONInvoicePaymentPopupView *)popupView {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)invoicePopupConfirm:(WONInvoicePaymentPopupView *)popupView {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    if (!indexPath) {
        return;
    }

    WONInvoiceContainer *container = [self.dataSource objectForIndexPath:indexPath];
    if (popupView.type == WONPopupTypeInvoiceReceived) {
        if (!self.paymentInvoice) {
            return;
        }
        popupView.firstBtn.enabled = NO;
        popupView.secondBtn.enabled = NO;
        @weakify(self);
        [self.paymentInvoice confirmInvoiceWithInvoiceId:container.invoice.invoiceId completionBlock:^{
            [popupView hidePopup];
            @strongify(self);
            [[self successPopupWithInvoiceContainer:container] showPopup];
            [self.dataSource forceReloadInvoices];
        } errorBlock:^(NSError *error) {
            [[WONNotificationBar notificationBarError] presentNotificationBarWithError:error];
        }];
    }
}

- (void)invoicePopupReject:(WONInvoicePaymentPopupView *)popupView {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    if (!indexPath) {
        return;
    }
    WONInvoiceContainer *container = [self.dataSource objectForIndexPath:indexPath];
    popupView.firstBtn.enabled = NO;
    popupView.secondBtn.enabled = NO;
    @weakify(self);
    [container.invoice rejectInvoiceWithCompletionBlock:^{
        [popupView hidePopup];
        @strongify(self);
        [self.dataSource forceReloadInvoices];
    } errorBlock:^(NSError *error) {
        if (error) {
            [[WONNotificationBar notificationBarError] presentNotificationBarWithError:error];
        }
    }];
}

@end
