

#import <UIKit/UIKit.h>

#import "WONPaginatedListViewController.h"
#import "WONInvoicesDataSource.h"

@interface WONInvoicesViewController : WONPaginatedListViewController  <WONTableViewDataSourceProtocol>

@property (strong, nonatomic) WONInvoicesDataSource *dataSource;

@end
