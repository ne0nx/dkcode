

#import "WONPopupView.h"
#import "WONRoundedButton.h"

@class WONInvoicePaymentPopupView;

typedef NS_ENUM(NSInteger, WONPopupType) {
    WONPopupTypeInvoiceRejectedOrAccepted,
    WONPopupTypeInvoiceReceived,
    WONPopupTypeInvoiceOutgoing,
    WONPopupTypeInvoiceCustom = WONPopupTypeInvoiceRejectedOrAccepted
};

@protocol WONInvoicePaymentPopupDelegate <NSObject>

@optional
- (void)invoicePopupWillOpen:(WONInvoicePaymentPopupView *)popupView;
- (void)invoicePopupDidClose:(WONInvoicePaymentPopupView *)popupView;
- (void)invoicePopupConfirm:(WONInvoicePaymentPopupView *)popupView;
- (void)invoicePopupReject:(WONInvoicePaymentPopupView *)popupView;

@end

@interface WONInvoicePaymentPopupView : WONPopupView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@property (weak, nonatomic) IBOutlet WONRoundedButton *firstBtn;
@property (weak, nonatomic) IBOutlet UIButton *secondBtn;

@property (weak,   nonatomic) id<WONInvoicePaymentPopupDelegate> delegate;
@property (assign, nonatomic) WONPopupType type;

+ (instancetype)popupViewWithType:(WONPopupType)type dismissCompletion:(WONPopupDismissCompletion)dismissCompletion;

@end