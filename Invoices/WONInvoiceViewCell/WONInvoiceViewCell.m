

#import "WONInvoiceViewCell.h"

@implementation WONInvoiceViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.timeLabel.font         = [UIFont wonOpenSansWithStyle:WONFontOpenSansStyleRegular andSize:12.0f];
    self.titleLabel.font        = [UIFont wonOpenSansWithStyle:WONFontOpenSansStyleRegular andSize:15.0f];
    self.amountLabel.font       = [UIFont wonOpenSansWithStyle:WONFontOpenSansStyleRegular andSize:15.0f];
    
    self.timeLabel.textColor    = [UIColor wonNotificationCellTimeColor];
    self.titleLabel.textColor   = [UIColor wonNotificationCellTitleColor];
    
    self.backgroundView         = [[UIImageView alloc] initWithImage:[UIImage imageWithColor:[UIColor clearColor]]];
    self.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageWithColor:[UIColor wonButtonHighlightGrayColor]]];
    
    //[self configureReactions];
}

#pragma mark Private
//- (void)reloadData
//{
//    if (self.history.isTransferToMe){
//        self.amountLabel.textColor = [UIColor wonHistoryTransferToMeColor];
//    } else {
//        self.amountLabel.textColor = [UIColor wonHistoryTransferFromMeColor];
//    }
//    
//    self.amountLabel.text = self.history.formattedAmount;
//    self.titleLabel.text = self.history.formattedTitle;    
//    self.timeLabel.text = [self.history formattedUpdatedDate];
//    
//    NSArray *statusImages = @[@"history_accepted_icon", @"history_waiting_icon", @"history_waiting_icon", @"history_aborted_icon", @"history_aborted_icon", @"history_aborted_icon"];
//    
//    self.statusImageView.image = [UIImage imageNamed:[statusImages objectAtIndex:self.history.entryStateId]];
//}
//
//- (void)configureReactions
//{
//    @weakify(self);
//    [[RACObserve(self, history) filter:^BOOL(id value) {
//        @strongify(self);
//        return self.history != nil;
//    }] subscribeNext:^(id x) {
//        @strongify(self);
//        [self reloadData];
//    }];
//}


#pragma mark Class methods

+ (CGFloat)cellHeight
{
    return 60;
}

@end
