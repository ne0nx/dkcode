

#import <UIKit/UIKit.h>
#import "WONTableViewCell.h"

@interface WONInvoiceViewCell : WONTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *statusImageView;

@end
