

#import "WONInvoice.h"

@interface WONInvoiceContainer : NSObject

@property (nonatomic, strong) WONInvoice *invoice;

+ (instancetype)containerWithInvoice:(WONInvoice *)invoice;

- (NSString *)formattedTitle;
- (NSString *)formattedAmount;
- (NSString *)formattedTime;
- (NSString *)formattedCurrencyDeclensionWithCount:(NSInteger)count;

- (UIColor *)invoiceColorFromState;
- (UIImage *)invoiceImageFromState;

@end
