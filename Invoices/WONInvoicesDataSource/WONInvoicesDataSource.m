

#import "WONInvoicesDataSource.h"
#import "WONInvoicesApiManager.h"
#import "WONInvoice.h"

@implementation WONInvoicesDataSource

#pragma mark - Public

- (void)forceReloadInvoices {
    [self startLoading];

    WONPage page = WONPageMake(1, _itemsPerPage * _currentPage);
    [WONInvoicesApiManager invoicesListWithPage:page completion:^(WONInvoicesListResponse *response, NSError *error) {
        if (error) {
            [[WONNotificationBar notificationBarError] presentNotificationBarWithError:error];
        }

        _totalCount = response.totalCount;

        [_loadedItems removeAllObjects];
        [self.arrayOfSectionDescriptions removeAllObjects];

        [self addItemsToOutput:response.items];

        [self finishLoading];
        [self.delegate dataSourceDidChanged:self];
    }];
}

- (WONInvoiceContainer *)objectForIndexPath:(NSIndexPath *)indexPath {
    return [super objectForIndexPath:indexPath];
}

#pragma mark - Private

- (void)loadData
{
    [self loadNextPage];
}

- (void)loadCurrentPageWithItemsNumber:(NSInteger)numberOfItems {
    WONPage page = WONPageMake(_currentPage, numberOfItems);
    [self startLoading];
    [WONInvoicesApiManager invoicesListWithPage:page completion:^(WONInvoicesListResponse *response, NSError *error) {
        if (error) {
            [[WONNotificationBar notificationBarError] presentNotificationBarWithError:error];
        }

        _totalCount = response.totalCount;

        [self addItemsToOutput:response.items];

        [self finishLoading];
        [self.delegate dataSourceDidChanged:self];
    }];
}

- (void)addItemsToOutput:(NSArray *)items {
    if (items == nil || items.count == 0) {
        return;
    }

    NSMutableArray *containers = [NSMutableArray array];
    for (WONInvoice *invoice in items) {
        WONInvoiceContainer *container = [WONInvoiceContainer containerWithInvoice:invoice];
        [containers addObject:container];
    }

    WONSectionDescriptionObject *section;
    section = [[WONSectionDescriptionObject alloc] initWithTitleString:nil rowsArray:containers];
    [self.arrayOfSectionDescriptions addObject:section];
}

@end
