

#import "WONInvoiceContainer.h"

#import "UIImage+ColoredImage.h"

@implementation WONInvoiceContainer

+ (instancetype)containerWithInvoice:(WONInvoice *)invoice
{
    WONInvoiceContainer *container = [[self class] new];
    container.invoice = invoice;
    return container;
}

#pragma mark -

- (NSString *)formattedTitle
{
    return [NSString stringWithFormat:LOC(@"invoice.title"),
            self.invoice.invoiceId,
            self.invoice.userTitle,
            [self formattedAmountWithCurrencyDeclension]];
}

- (NSString *)formattedAmount
{
    return [[WONCurrencyFormatter sharedInstance] formattedAmount:self.invoice.amount withCurrency:self.invoice.currencyId];
}

- (NSString *)formattedAmountWithCurrencyDeclension
{
    NSString *currencyDeclension = [self formattedCurrencyDeclensionWithCount:self.invoice.amount];
    return [[WONCurrencyFormatter sharedInstance] formattedAmount:self.invoice.amount withCurrencySymbol:currencyDeclension];
}

- (NSString *)formattedTime
{
    return [self.invoice.createDate wonTimeAgo];
}

- (NSString *)formattedCurrencyDeclensionWithCount:(NSInteger)count;
{
    return TTTLocalizedPluralStringForLanguage(count, @"invoice.declension.currency", @"ru");
}

- (UIColor *)invoiceColorFromState {
    UIColor *color = nil;
    switch (self.invoice.state) {
        case WONInvoiceStateAccepted:
            color = [UIColor wonInvoiceAcceptedColor];
            break;
        case WONInvoiceStateCanceled:
        case WONInvoiceStateExpired:
        case WONInvoiceStateRejected:
            color = [UIColor wonInvoiceRejectedColor];
            break;
        case WONInvoiceStateCreated:
        case WONInvoiceStateProcessing:
        case WONInvoiceStateReceived:
            color = [UIColor wonInvoiceProcessingColor];
        default:
            break;
    }
    return color;
}

- (UIImage *)invoiceImageFromState {
    NSString *imageName = nil;
    switch (self.invoice.direction) {
        case WONInvoiceDirectionInc:
            imageName = @"invoice_down_arrow_icon";
            break;
        case WONInvoiceDirectionOut:
            imageName = @"invoice_up_arrow_icon";
            break;
        default:
            break;
    }
    return [UIImage coloredImageNamed:imageName color:[self invoiceColorFromState]];
}

@end
