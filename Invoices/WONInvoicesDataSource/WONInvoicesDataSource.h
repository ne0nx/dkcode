

#import "WONBaseDataSource.h"
#import "WONInvoiceContainer.h"

@interface WONInvoicesDataSource : WONBaseDataSource

- (void)forceReloadInvoices;

- (WONInvoiceContainer *)objectForIndexPath:(NSIndexPath *)indexPath;

@end
